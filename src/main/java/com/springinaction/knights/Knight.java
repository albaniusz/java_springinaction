package com.springinaction.knights;

/**
 * Knight
 */
public interface Knight {
	void embarkOnQuest();
}
