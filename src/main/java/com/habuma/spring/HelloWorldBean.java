package com.habuma.spring;

/**
 * HelloWorldBean
 */
public class HelloWorldBean {

	public String sayHello() {
		return "HelloWorld";
	}
}
