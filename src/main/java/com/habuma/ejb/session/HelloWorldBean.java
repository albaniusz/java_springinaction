package com.habuma.ejb.session;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

/**
 * HelloWorldBean
 */
public class HelloWorldBean implements SessionBean {

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void ejbRemove() {
	}

	public void setSessionContext(SessionContext ctx) {
	}

	public String sayHello() {
		return "HelloWorld";
	}

	public void ejbCreate() {
	}
}
